<?php

namespace EnjoyFlowersApis;

class ReadCSVFile
{
    private static function checkIfMehtodExist($key)
    {
        $values = array(
            'update_shipments' => true,
        );

        if (isset($values[$key])) {
            return $values[$key];
        }
        return false;
    }

    public static function convertToArrayWithKeys( $csv_data = array() ){
        $func = function($valor) {
            $str = str_replace( array( ' ', '.'), '_', strtolower( $valor ) );
            return $str;
        };
        $keys = array_map( $func, $csv_data[0] );
        $limit = count( $keys );
        array_shift( $csv_data );
        $temp = array();
        $list_formated = array();
        foreach ($csv_data as $tracking) {
            for ($_c=0; $_c < $limit; $_c++) {
                $temp[$keys[$_c]] = $tracking[$_c];
            }
            $_list_formated[] = $temp;
        }
        return $_list_formated;
    }

    public static function walkingArray( $method = '', $submit = 'procesar', $array_file_data = array() ){
        if ($method === '' || empty($array_file_data) || !self::checkIfMehtodExist($method))
            return array();
        switch ($method) {
            case 'update_shipments':
				switch($submit){
					case 'preview':
						return self::getCratejoyShipment($array_file_data);
						break;
					case 'procesar':
						return self::upCratejoyShipment($array_file_data);
						break;
				}
                break;

            default:
                return array();
                break;
        }
    }

    private static function getCratejoyShipment($data_to_update = array())
    {
        $cratejoy = new CrateJoy;
        $keys = array(
            1 => 'tracking_number',
            2 => 'id',
            3 => 'status',
            4 => 'adjusted_ordered_at',
            6 => array(
                'field_key' => 'customer',
                'looking_for' => 'name'
            ),
            'looks_in_arrays' => false
        );
        $result_process = array();
        $result_process['success']['header'] = array(
            'Shipment ID',
            'customer',
            'Tracking Number',
            'Adjusted Ordered At',
            'Status',
        );
        $_c = -1;

        foreach ($data_to_update as $data) {
            $_c++;

            if (!isset($data['id'])) {
                $result_process['alertas'][$_c]['shipment_id'] = '---';
                $result_process['alertas'][$_c]['msg'] = 'No tiene Shipment ID.';
                continue;
            }

            $response_request = $cratejoy->getShipmentInfo($keys, $data['id']);

            if (!isset($response_request['error_createjoy'])) {
                $result_process['success']['results'][$_c] = $response_request;
            } else {
                $result_process['alertas'][$_c]['shipment_id'] = $data['id'];
                $result_process['alertas'][$_c]['msg'] = 'Error al hacer la petición.';
            }
        }
        return $result_process;
    }
    private static function upCratejoyShipment($data_to_update = array())
    {
        $cratejoy = new CrateJoy;
        $keys = array(
            1 => 'tracking_number',
            2 => 'id',
            3 => 'status',
            4 => 'adjusted_ordered_at',
            6 => array(
                'field_key' => 'customer',
                'looking_for' => 'name'
            ),
            'looks_in_arrays' => false
        );
        $result_process = array();
        $result_process['success']['header'] = array(
            'Shipment ID',
            'customer',
            'Tracking Number',
            'Adjusted Ordered At',
            'Status',
        );
        $_c = -1;

        foreach ($data_to_update as $data) {
            $_c++;

            if (!isset($data['tracking_number']) && !isset($data['target_ship_date'])) {
                $result_process['alertas'][$_c]['shipment_id'] = $data['id'];
                $result_process['alertas'][$_c]['msg'] = 'No tiene Tracking Number o Target Ship Date para actualizar.';
                continue;
            }
            if (isset($data['tracking_number']) && $data['tracking_number'] == '' && !isset($data['target_ship_date'])) {
                $result_process['alertas'][$_c]['shipment_id'] = $data['id'];
                $result_process['alertas'][$_c]['msg'] = 'No tiene Tracking Number.';
                continue;
            }
            if ((isset($data['target_ship_date']) && !Utils::validateDate($data['target_ship_date'], 'd/m/Y'))) {
                $result_process['alertas'][$_c]['shipment_id'] = $data['id'];
                $result_process['alertas'][$_c]['msg'] = 'El formato de fecha no es valido debe ser d/m/Y.';
                continue;
            }

            $array_to_use_in_rq = $cratejoy->createArrayToUpdateShipment($data);
            $response_request = $cratejoy->createConnection($array_to_use_in_rq, true);

            if (!isset($response_request['error_createjoy'])) {
                $result_process['success']['results'][$_c] = Utils::getValuesByKeys($response_request, $keys);
            } else {
                $result_process['alertas'][$_c]['shipment_id'] = $data['id'];
                $result_process['alertas'][$_c]['msg'] = 'Error al hacer la petición.';
            }
        }
        return $result_process;
    }
}

?>