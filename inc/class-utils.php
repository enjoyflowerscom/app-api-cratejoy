<?php

namespace EnjoyFlowersApis;
use \DateTime;

class Utils {

    /**
     * [get_value_by_key_inarray Recursive function, walk array for get value in specific key_name]
     * @param  array   $info         [array to walk]
     * @param  string  $keySearch    [key_field]
     * @param  boolean $return_array [if field is array, return array found it]
     * @param  boolean $looks_in_arrays []
     * @return [any]                [value]
     */
    public static function get_value_by_key_inarray( $info = array(), $keySearch = '', $return_array = false, $looks_in_arrays = true ){
        if( $keySearch == '' || !is_array( $info ) || empty( $info ) )
            return;
        $value = '';
        $value = $looks_in_arrays ? self::search_value_by_key( $info, $keySearch ) : '';
        if( $value != '' ){
            return $value;
        }else{
            foreach ( $info as $key => $item ) {
                if ( !is_int( $key) && $key == $keySearch && is_array( $item ) && $return_array ) {
                    return $item;
                }else if ( !is_int( $key) && $key == $keySearch ) {
                    return $item;
                }else if ( $looks_in_arrays && is_array( $item ) ) {
                    $value = self::search_value_by_key( $item, $keySearch );
                    if( !$value ){
                        $value = self::get_value_by_key_inarray( $item, $keySearch );
                    }
                    if( $value != '' ){
                        break;
                    }
                }
            }
        }
        return $value;
    }

    /**
     * [search_value_by_key this function only working with arrays with max 2 levels]
     * @param  [array] $array_data [array to search value by key]
     * @param  [string] $keySearch  [key to search]
     * @return [array]             [array with all datas found]
     */
    public static function search_value_by_key( $array_data, $keySearch ){
        if( $keySearch == '' || !is_array( $array_data ) || empty( $array_data ) )
            return '';
        $value = array_column( $array_data, $keySearch );
        return current( $value );
    }


    public function addToArray( $data, $array_base, $key, $keyNewArray, $is_merge_fields = false ){
        $temp_array = array();
        if( !isset($data[$key]) ){
            return array_merge( $array_base, $temp_array );
        }

        if( $is_merge_fields ){
            $temp_array['merge_fields'][$keyNewArray] = is_bool( $data[$key] ) ? ( $data[$key] ? 'true' : 'false') : $data[$key];
        }else{
            $temp_array[$keyNewArray] = is_bool( $data[$key] ) ? ( $data[$key] ? 'true' : 'false') : $data[$key];
        }
        return array_merge_recursive( $temp_array,$array_base );
    }

    /**
     * getValuesByKeys function
     * Esta funcion permite obtener el valor de un campo en un array, buscando el "key" dentro del []
     * además permite buscar de forma recursiva, es decir si el valor final está dentro de otro [] para
     * ello debe crear una estructura como esta a buscar
     * array(
     *   'field_key' => 'product',
     *   'looking_for' => 'name',
     *   'looks_in_arrays' => false
     * ),
     * @param array $_response
     * @param array $_fields
     * @return void
     */
    public function getValuesByKeys( $_response = array(), $_fields = array(), $looks_in_arrays = true ){
        if( empty( $_response ) || empty( $_fields ) )
            return NULL;
        $array_response = array();
        $looks_in_arrays = isset($_fields['looks_in_arrays']) ? $_fields['looks_in_arrays'] : true;
        // echo '<prev>';
        foreach ( $_fields as $key => $key_to_search ) {
            if($key == 'looks_in_arrays')
                continue;
            if( is_array( $key_to_search ) ){
                $temp_array = self::get_value_by_key_inarray( $_response, $key_to_search['field_key'], true );
                $return_array = isset($key_to_search['return_array']) ? $key_to_search['return_array'] : false;
                $looks_in_arrays = isset($key_to_search['looks_in_arrays']) ? $key_to_search['looks_in_arrays'] : false;
                $array_response[$key_to_search['field_key']] = self::get_value_by_key_inarray( $temp_array, $key_to_search['looking_for'], $return_array, $looks_in_arrays );
            }else if( !isset( $array_response[$key_to_search] ) ){
                $array_response[$key_to_search] = self::get_value_by_key_inarray( $_response, $key_to_search, false, $looks_in_arrays );
            }
            // echo "array_response -> <br/>";
            // var_dump($array_response[$key_to_search]);
            // echo "<br/> <- array_response <br/><br/>";
            continue;
        }
        // echo '</prev>';
        return $array_response;
    }

    public function validateEmail ( $email ){
        $email = str_replace( ' ', '', $email );
        $email = filter_var( $email, FILTER_SANITIZE_EMAIL);
        return filter_var( $email, FILTER_VALIDATE_EMAIL );
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

 ?>