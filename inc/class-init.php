<?php
namespace EnjoyFlowersApis;

class Init
{

    private static $default_files = array(
        'class-utils',
        'class-config',
        'class-cratejoy',
        'class-readCSVFile',
    );

    private static $instance;


    public static function get_instance()
    {

        if (!isset(self::$instance)) {
            self::$instance = new Init;
            self::$instance->load_files();
            if(isset($_POST['file_path'])) {
                $array_file_data = self::$instance->readCSV($_POST['file_path']);
                if ($array_file_data && file_exists($_POST['file_path'])) {
                    if (!unlink($_POST['file_path'])) {
                        trigger_error('Error eliminando el archivo');
                    }
                }
                return self::runOperation($_POST, $_POST['subject'], $array_file_data);
            }else{
                if ($file_data = self::uploaded_file()) {
                    $array_file_data = self::$instance->readCSV($file_data);
                    return self::runOperation($_POST, $_POST['subject'], $array_file_data);
                } else {
                    trigger_error('Ha ocurrido un error con el archivo verifique la Extensión y que no este dañado.');
                }
            }

        }
        return self::$instance;

    }

    public static function getFileName(){
        return self::uploaded_file();
    }

    private static function uploaded_file()
    {
        $file = $_FILES;
        if (isset($file['file']) && $file['file']['name'] != '') {
            return self::validateFile($file);
        }
    }

    private static function validateFile($_file = null)
    {
        if (!isset($_file)) {
            return FALSE;
        }
        if (isset($_file['file']) && $_file['file']['name'] != '') {
            $file_name = $_file['file']['name'];
            $temp_name = $_file['file']['tmp_name'];
            $file_type = $_file['file']['type'];
            $base = basename($file_name);
            $extension = substr($base, strlen($base) - 4, strlen($base));

            $allowed_extensions = array(".csv");
            if (in_array($extension, $allowed_extensions)) {
                $target = 'csv_upload/' . $base;
                $target_path = getcwd() . '/csv_upload/' . $base;
                /* if (file_exists($target)) {
                    unlink($target);
                } */
                move_uploaded_file($temp_name, $target);
                return $target_path;
            } else {
                trigger_error('Formato de archivo invalido.');
                exit;
            }
        }
    }

    private static function readCSV($temp_name = '')
    {
        if ($temp_name === '') return FALSE;
        $list_trackings = array();
        $file = fopen($temp_name, "r");
        while (!feof($file)) {
            $list_trackings[] = fgetcsv($file);
        }
        fclose($file);
        clearstatcache();

        if (empty($list_trackings) || count($list_trackings) == 1)
            return FALSE;

        return $list_trackings;
    }

    public static function runOperation($values_form = array(), $method = 'procesar', $csv_ups_loaded = array())
    {
        if (empty($values_form) || empty($csv_ups_loaded)) {
            return false;
        }
        if (!empty($csv_ups_loaded) && is_array($csv_ups_loaded)) {
            $list_formated = ReadCSVFile::convertToArrayWithKeys($csv_ups_loaded);
            $result = ReadCSVFile::walkingArray( $values_form['operation'], $method, $list_formated );
            return $result;
        }
        trigger_error('Ha Ocurrido un error el dato ingresado no es un array o está vacío.');
        return FALSE;
    }

    public static function load_files($files = array())
    {

        $require_files = array_merge(self::$default_files, $files);
        foreach ($require_files as $key => $class) {
            require_once 'inc/' . $class . '.php';
        }
    }
}

?>