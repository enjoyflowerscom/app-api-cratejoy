<?php
namespace EnjoyFlowersApis;

use EnjoyFlowersApis\Utils;

class CrateJoy
{

    /**
     * [$fields_request_cratejoy son los nombres de los campos del json que responde cratejoy]
     * @var array
     */
    private static $fields_request_cratejoy = array(
        0 => 'email',
        1 => 'customer_id',
        2 => 'first_name',
        3 => 'name',
        4 => 'autorenew',
        5 => 'is_gift',
        6 => array(
            'field_key' => 'term',
            'looking_for' => 'name'
        ),
        7 => array(
            'field_key' => 'product',
            'looking_for' => 'name'
        ),
    );

    public static function getFieldsRequestCratejoy()
    {
        return self::$fields_request_cratejoy;
    }

    private static function getAccess()
    {
        $data_access = Config::get('CRATEJOY_USERNAME') . ':' . Config::get('CRATEJOY_API_KEY');
        return $data_access;
    }

    private static function getPath($key)
    {
        $base_url = Config::get('CRATEJOY_BASE_PATH');
        $values = array(
            'get_all_shipments' => $base_url . 'shipments/',
            'get_shipment_by_id' => $base_url . 'shipments/{shipment_id}/',
            'update_shipment' => $base_url . 'shipments/{shipment_id}/',
            'search_customer_by_custom_field' => $base_url . 'orders/?{fields}',
            'get_customer_by_id' => $base_url . 'customers/{customer_id}/',
            'get_an_order' => $base_url . 'orders/{order_id}/',
            'get_an_subcr' => $base_url . 'subscriptions/{sub_id}/',
        );

        if (isset($values[$key])) {
            return $values[$key];
        }
    }

    private static function getTypeConnect($key)
    {
        $values = array(
            'get_all_shipments' => 'GET',
            'get_shipment_by_id' => 'GET',
            'update_shipment' => 'PUT',
            'search_customer_by_custom_field' => 'GET',
            'get_customer_by_id' => 'GET',
            'get_an_order' => 'GET',
            'get_an_subcr' => 'GET',
        );

        if (isset($values[$key])) {
            return $values[$key];
        }
        return FALSE;
    }

    /**
     * [replaceByFilter este toma la url de la consulta creada en getPath, y dependiendo del tipo de filtro se reemplaza los datos en ella]
     * @param  [string] $_list_id   [ID de la lista]
     * @param  [string] $filter     [Tipo de filtro]
     * @param  string $_filter_by [Si el tipo es buscar un usuario en especifico se filtra por algun campo del usuario]
     * @param  array $fields [Es la información del nuevo usuario]
     * @return [string]             [Url final con la que se hará la consulta]
     */
    private static function replaceByFilter($__shipment_id = 0, $filter, $_fields = array())
    {
        $url_request = self::getPath($filter);
        switch ($filter) {
            case 'get_all_shipments':
                break;
            case 'search_customer_by_custom_field':
                if (empty($_fields)) {
                    trigger_error('NO hay campos para hacer la busqueda');
                    exit;
                }
                $url_request = str_replace('{fields}', http_build_query($_fields), $url_request);
                break;
            case 'get_customer_by_id':
                if (empty($_fields) || !isset($_fields['customer_id'])) {
                    trigger_error('NO hay Customer ID para hacer la consulta');
                    exit;
                }
                $url_request = str_replace('{customer_id}', $_fields['customer_id'], $url_request);
                break;
            case 'get_an_order':
                if (empty($_fields) || !isset($_fields['order_id'])) {
                    trigger_error('NO hay Order ID para hacer la consulta');
                    exit;
                }
                $url_request = str_replace('{order_id}', $_fields['order_id'], $url_request);
                break;
            case 'get_an_subcr':
                if (empty($_fields) || !isset($_fields['sub_id'])) {
                    trigger_error('NO hay Subscription ID para hacer la consulta');
                    exit;
                }
                $url_request = str_replace('{sub_id}', $_fields['sub_id'], $url_request);
                break;
            case 'update_shipment':
                if ($__shipment_id == 0) {
                    return;
                }
                $url_request = str_replace('{shipment_id}', $__shipment_id, $url_request);
                (!empty($_fields))
                    and $_json_data = json_encode($_fields);
                break;
            case 'get_shipment_by_id':
            default:
                if ($__shipment_id == 0) {
                    return;
                }
                $url_request = str_replace('{shipment_id}', $__shipment_id, $url_request);
                break;
        }
        $info_to_use_in_connection = array(
            'url_request' => $url_request,
            'json_data' => isset($_json_data) ? $_json_data : '',
        );
        return $info_to_use_in_connection;
    }

    private static function doConnection($info_to_use_in_connection = array(), $return_array)
    {

        if (!$info_to_use_in_connection['url_request']) {
            trigger_error('Ha ocurrido un error NO hay URL para hacer el request');
            exit;
        }
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $info_to_use_in_connection['url_request']);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_handle, CURLOPT_HEADER, FALSE);
        curl_setopt($curl_handle, CURLOPT_USERPWD, $info_to_use_in_connection['data_access']);
        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json"
        ));

        if ($info_to_use_in_connection['json_data'] != '') {
            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $info_to_use_in_connection['json_data']);
        }
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, $info_to_use_in_connection['type_connect']);

        $response = curl_exec($curl_handle);
        curl_close($curl_handle);

        $json_rq = $response;
        $response = json_decode($response, TRUE);

        if ($return_array) {
            return $response;
        }

        if (isset($response['errors']) && $response['errors'][0] != '') {
            var_dump($info_to_use_in_connection['type_connect']);
            var_dump($json_rq);

            $msg['error_createjoy'] = 0;
            $msg['msg'] = 'La consulta a Cratejoy' . $info_to_use_in_connection['url_request'] . ' ha returnado el siguiente error : ' . $response['errors'][0];
            return $msg;
        }
        return $response;
    }

    public static function createConnection($args = NULL, $return_array = false)
    {
        $info_to_use_in_connection = array();
        $type_connect = self::getTypeConnect($args['method']);
        if (!isset($args['method']) || !$type_connect) {
            trigger_error('El tipo de petición no existe.');
            exit;
        }

        $filter_by = $args['method'];
        $_shipment_id = isset($args['shipment_id']) ? $args['shipment_id'] : 0;
        $_fields = isset($args['fields']) ? $args['fields'] : array();

        $info_to_use_in_connection = self::replaceByFilter( /*'792069063'*/$_shipment_id, $args['method'], $_fields);

        $info_to_use_in_connection['data_access'] = self::getAccess();
        $info_to_use_in_connection['type_connect'] = $type_connect;

        $response = self::doConnection($info_to_use_in_connection, $return_array);
        return $response;
    }

    /**
     * [getInfo Get the info from cratejoy]
     * @param  [array] $fields      [Name Fields Keys to search in cratejoy]
     * @param  [number] $shipment_id [ID_shipment]
     * @param  string $method      [request type]
     * @return [array]              [array with values found it]
     */
    public static function getShipmentInfo($fields, $shipment_id = NULL, $method = 'get_shipment_by_id')
    {
        if (!isset($shipment_id)) {
            trigger_error('Se necesita el ID del shipment');
            exit;
        }
        $args = array(
            'method' => $method,
            'shipment_id' => $shipment_id,
        );
        $info = array();
        $_response = self::createConnection($args);

        if (isset($_response['error_createjoy']) && $_response['error_createjoy'] == 0) {
            return $_response;
        }
        $init_values = Utils::getValuesByKeys($_response, $fields);
        return $init_values;
    }


    public static function checkIfIsGift($shipmentInfo = array())
    {
        $order_obj = self::getOrderInShipmentInfo($shipmentInfo);
        if ($order_obj['is_gift'])
            return TRUE;
        return FALSE;
    }

    private static function getReceiverInfo($shipmentInfo = array())
    {
        $order = self::getOrderInShipmentInfo($shipmentInfo);
        $order_id = self::getData($order, 'id', false, false);
        if (!$order_id) {
            return array();
        }
        $order_obj = self::getInfoOrderById(array(), (int)$order_id);
        $receiver_name = Utils::getValuesByKeys($order_obj, array(
            0 => array(
                'field_key' => 'order_gift_info',
                'looking_for' => 'gift_recipient_name'
            )
        ));
        $receiver_email = Utils::getValuesByKeys($order_obj, array(
            0 => array(
                'field_key' => 'order_gift_info',
                'looking_for' => 'gift_recipient_email'
            )
        ));
        $receiver_info = array(
            'gift_recipient_name' => $receiver_name['order_gift_info'],
            'gift_recipient_email' => $receiver_email['order_gift_info'],
        );
        return $receiver_info;
    }

    private static function getOrderInShipmentInfo($shipmentInfo = array())
    {
        $order_obj = self::getData($shipmentInfo, 'order', $return_array = true);
        return $order_obj;
    }

    private function getData($_array, $_arrayToSearch, $return_array = false, $looks_in_arrays = true)
    {
        $data = Utils::get_value_by_key_inarray($_array, $_arrayToSearch, $return_array, $looks_in_arrays);
        return $data;
    }

    public function getCustomerId($array = array())
    {
        return self::getData($array, 'customer_id');
    }

    public function getLastOrderId($array = array())
    {
        return self::getData($array, 'order_id');
    }

    public function getLastSubscriptionId($array = array())
    {
        return self::getData($array, 'subscription_id');
    }

    public function getInfoOrderById($customerInfo = array(), $order_id = 0)
    {
        $order_id = !empty($customerInfo) ? self::getLastOrderId($customerInfo) : ($order_id && $order_id != 0 ? $order_id : null);
        if (!isset($order_id))
            return NULL;
        $args = array(
            'method' => 'get_an_order',
            'fields' => array(
                'order_id' => $order_id,
            ),
        );
        return self::createConnection($args, true);
    }

    public function getSubscriptionById($orderInfo)
    {
        $sub_id = self::getLastSubscriptionId($orderInfo);
        if (!isset($sub_id))
            return NULL;
        $args = array(
            'method' => 'get_an_subcr',
            'fields' => array(
                'sub_id' => $sub_id,
            ),
        );
        return self::createConnection($args, true);
    }
    public function searchCustomerByEmail($email)
    {
        $args = array(
            'method' => 'search_customer_by_custom_field',
            'fields' => array(
                'customer.email' => $email
            ),
        );
        return self::validateResponse(self::createConnection($args, true));
    }

    private function validateResponse($response)
    {
        $stop = FALSE;
        if (empty($response['results']))
            return NULL;
        return $response;
    }

    public static function validateCustomerInfo($data)
    {
        if ($data['email'] == '' || $data['customer_id'] == '') {
            return FALSE;
        }
        return TRUE;
    }

    public static function validateGiftInfo($data)
    {
        if (!isset($data['gift_recipient_email']) || !isset($data['gift_recipient_name']) || !$data['gift_recipient_email'] || !$data['gift_recipient_name']) {
            return FALSE;
        }
        return TRUE;
    }

    public static function createArrayToUpdateShipment($values = array())
    {
        if (empty($values)) {
            return false;
        }
        $data_to_use = array(
            'method' => 'update_shipment',
            'shipment_id' => $values['id'],
            'fields' => array()
        );
        if (isset($values['target_ship_date'])) {
            $u = strtotime(str_replace('/', '-', $values['target_ship_date']));
            $data_to_use['fields']['adjusted_ordered_at'] = date('Y-m-d 00:00:00', $u) . 'Z';
        }
        if (isset($values['tracking_number'])) {
            $data_to_use['fields']['status'] = 'shipped';
            $data_to_use['fields']['tracking_number'] = $values['tracking_number'];
        }
        return $data_to_use;
    }
}


?>
