<?php 

namespace EnjoyFlowersApis;

class Config {

    private static $credentials = array(
        /*Mailchimp*/
        'MAILCHIMP_USERNAME'               => 'admin',
        // 'MAILCHIMP_API_KEY'                => '2f8015b61fbcf9b106c703c61c8550a5-us13',
        'MAILCHIMP_API_KEY'                => '15edd8636e21dfb81018fef2b36ac5ce-us13',
        //http                             ://developer.mailchimp.com/documentation/mailchimp/guides/get-started-with-mailchimp-api-3/#authentication
        'MAILCHIMP_PREFIX_URL_CONNECT'     => 'https://',
        'MAILCHIMP_SUFIX_URL_CONNECT'      => '.api.mailchimp.com/3.0/',
        // 'MAILCHIMP_CUSTOMER_ID_LIST'    => '98014f321f', // List "Test Api Connect " DEV
        'MAILCHIMP_DEFAULT_ID_LIST'        => '98014f321f', // List "Test Api Connect " DEV
        'MAILCHIMP_CUSTOMER_ID_LIST'       => '7ab8107033', // List "Customers - April 2017 " PROD
        'MAILCHIMP_GIFT_RECEIVERS_ID_LIST' => '76aa02fdcb', // List "Gift Receivers" PROD
        /*CrateJoy*/
        'CRATEJOY_USERNAME'                => 'floresentucasa',
        'CRATEJOY_API_KEY'                 => 'R8FJ8mYD7ZB4sLzZ',
        'CRATEJOY_BASE_PATH'               => 'https://api.cratejoy.com/v1/',
        'CRATEJOY_BASE_PATH_STORE'         => 'http://www.enjoyflowers.com/v1/store/api/',
    );

    private static $default = array();

    /**
     * Get the config value for a given key
     *
     * @param string $key
     */
    public static function get( $key ) {

        $values = array_merge( self::$default, self::$credentials );

        if ( isset( $values[ $key ] ) ) {
            return $values[ $key ];
        } else {
            trigger_error( '(KEY no exists), Invalid config value: ' . $key );
        }

    }

}

?>