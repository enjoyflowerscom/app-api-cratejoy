<?php
namespace EnjoyFlowersApis;

require_once dirname(__FILE__) . '/inc/class-init.php';
use EnjoyFlowersApis\Init as start;

$file_path = false;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $response = start::get_instance();
    if($_POST['subject'] == 'preview') {
        $file_path = start::getFileName();
    }
}
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8"><html>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Testing Api Conection Cratejoy</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/js/DataTables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css"/>
    <script type="text/javascript" src="assets/js/DataTables/datatables.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Api Cratejoy</h1>
        <?php if ($_SERVER["REQUEST_METHOD"] == "POST") { ?>
            <h2>Resultados</h2>


            <?php if (!empty($response['success'])) { ?>
                <div class="wrapper-content">
                    <h4>Shipments</h4>
                    <table class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <?php foreach ($response['success']['header'] as $value) { ?>
                                    <th><?php echo $value; ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($response['success']['results'] as $value) { ?>
                                <tr>
                                    <td><?php echo $value['id']; ?></td>
                                    <td><?php echo $value['customer']; ?></td>
                                    <td><?php echo $value['tracking_number']; ?></td>
                                    <td><?php echo $value['adjusted_ordered_at']; ?></td>
                                    <td><?php echo $value['status']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>

            <?php if (!empty($response['alertas'])) { ?>
                <div class="wrapper-content">
                    <h4>Alertas</h4>
                    <table class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Shipment ID</th>
                                <th>Mensaje</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($response['alertas'] as $value) { ?>
                                <tr>
                                    <td><?php echo $value['shipment_id']; ?></td>
                                    <td><?php echo $value['msg']; ?></td>
                                </tr>
                            <?php

                        } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
        <?php } ?>
        
        <form enctype="multipart/form-data" action="index.php" method="post" class="form-group">
            <div class="form-group">
                <label for="sel1">Select the Operation:</label>
                <select class="form-control" id="sel1" name="operation" required="required">
                    <option disabled>-Seleccione una opción-</option>
                    <option value="update_shipments">Update Shipments Cratejoy</option>
                </select>
            </div>
            <div class="form-group">
                <?php if (!$file_path) { ?>
                    <label>Adjunte el archivo CSV </label>
                    <input type="file" name="file" accept="application/csv" value="" required="required">
                <?php } ?>
                <?php if ($file_path) { ?>
                    <input class="hidden" type="hidden" name="file_path" accept="application/csv" value="<?php echo $file_path; ?>" required="required">
                <?php } ?>
            </div>
            <?php if (!$file_path) { ?>
                <button class="btn btn-default" name="subject" type="submit" value="preview">Pre-visualizar Datos</button>
            <?php } ?>
    		<button class="btn btn-default" name="subject" type="submit" value="procesar">Procesar Datos</button>
        </form>
        <br>
        <br>
    </div>
    <script>
        $(document).ready(function() {
            $('table.display').DataTable({
                colReorder: true
            });
        } );
    </script>
</body>
</html>