# Cratejoy
## Documentation

1. Shipment
    * [Methods](http://docs.cratejoy.com/docs/shipment-methods-1)
    * [Properties Datas Response](http://docs.cratejoy.com/v2.0/docs/shipment)
2. Batch
    * [Shipment Batch](http://docs.cratejoy.com/docs/shipment-batches)
    * [Shipment Batch Methods](http://docs.cratejoy.com/docs/shipment-batch-methods)

#### Datos Tarjeta de Pruebas para la tienda DEV
* [Tarjetas Test](https://help.cratejoy.com/hc/en-us/articles/201872029-How-Do-I-Place-a-Test-Order-On-My-Storefront-)


```
Request Access Key
client_id: floresentucasa
client_secret: R8FJ8mYD7ZB4sLzZ
```

# UPS
## Tracking API Connection

1. Links
    * [GET STARTED](https://www.ups.com/upsdeveloperkit?loc=en_US#)
    * [Request Access Key](https://www.ups.com/upsdeveloperkit/requestaccesskey?loc=en_US)
    * [All APIs](https://www.ups.com/upsdeveloperkit/backToPrevious?loc=en_US)
2. Developer Kit Guide Page
    * [Community](https://developerkitcommunity.ups.com/index.php/Main_Page)

```
Request Access Key
-Access Key: DD2D09B5D4213D88
-Access Key: 9D2D09FE2721E9F8

ID: ENJOYFLOWERSCO
Password: Colombia1
```

# Mailchimp
## API Connection

1. API Fundamentals
    * [Get/Create/Info About API Key](http://kb.mailchimp.com/integrations/api-integrations/about-api-keys)
    * [Doc](http://developer.mailchimp.com/documentation/mailchimp/)
2. Developer Guide
    * [References](http://developer.mailchimp.com/documentation/mailchimp/reference/overview/)
    * [Error Glossary](http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/)
3. Resources URL
    Estructure --https://<dc>.api.mailchimp.com/3.0--
    The <dc> part of the URL corresponds to the data center for your account. For example, if the last part of your MailChimp API key is us6, all API endpoints for your account are available at https://us6.api.mailchimp.com/3.0/

```
Login
username: camilo.villanueva@nivelics.com
pass: Nivelics2017*
```
